package pm.baoviet.customlinechart;

/**
 * Created by Kaz on 16:44 9/24/18
 */
public class TempDetail {

    int recordTime;
    String temperature;

    public TempDetail(String temperature) {
        recordTime = (int) (System.currentTimeMillis() / 1000);
        this.temperature = temperature;
    }
}
