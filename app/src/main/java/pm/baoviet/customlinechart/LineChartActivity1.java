
package pm.baoviet.customlinechart;

import android.graphics.DashPathEffect;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.MotionEvent;
import android.view.WindowManager;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.listener.ChartTouchListener;
import com.github.mikephil.charting.listener.OnChartGestureListener;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.Utils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import pm.baoviet.customlinechart.custom.MyAxisValueFormatter;
import pm.baoviet.customlinechart.custom.MyMarkerView;

public class LineChartActivity1 extends DemoBase implements OnChartGestureListener, OnChartValueSelectedListener {

    private LineChart chart;
    private List<TempDetail> tempDetails = new ArrayList<>();
    ArrayList<Entry> values;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_linechart);

        initData();

        chart = findViewById(R.id.chart1);
        chart.setOnChartGestureListener(this);
        chart.setOnChartValueSelectedListener(this);
        chart.setDrawGridBackground(false);

        chart.getDescription().setEnabled(false);
        chart.setTouchEnabled(true);
        chart.setDragEnabled(true);
        chart.setScaleXEnabled(false);
        chart.setScaleYEnabled(false);

        // if disabled, scaling can be done on x- and y-axis separately
        chart.setPinchZoom(true);

        IAxisValueFormatter iAxisValueFormatter = new MyAxisValueFormatter();
        XAxis xAxis = chart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);
        xAxis.setValueFormatter(iAxisValueFormatter);

        YAxis leftAxis = chart.getAxisLeft();
        leftAxis.removeAllLimitLines(); // reset all limit lines to avoid overlapping lines
        leftAxis.setAxisMaximum(41f);
        leftAxis.setAxisMinimum(35f);
        leftAxis.enableGridDashedLine(10f, 10f, 0f);
        leftAxis.setDrawZeroLine(true);
        // limit lines are drawn behind data (and not on top)
        leftAxis.setDrawLimitLinesBehindData(true);
        leftAxis.setLabelCount(5, true);
        chart.getAxisRight().setEnabled(false);
        chart.getAxisLeft().setDrawAxisLine(false);
        chart.setDrawBorders(false);

        MyMarkerView mv = new MyMarkerView(this, R.layout.custom_marker_view);

        mv.setChartView(chart); // For bounds control
        chart.setMarker(mv);
        // add data
        setData(60 * 24, 0);
//        chart.animateX(2500);
        //chart.invalidate();

        // get the legend (only possible after setting data)
        Legend l = chart.getLegend();
        // modify the legend ...
        l.setForm(Legend.LegendForm.NONE);
        // // dont forget to refresh the drawing
//        chart.moveViewToX();
//        chart.getXAxis().setGranularityEnabled(true);
//        chart.setScaleMinima(count, 1f);
//        chart.moveViewToX(60*4);
        long currmili = System.currentTimeMillis();
        Date d = new Date(currmili);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(d);

//        int moveToView = calendar.get(Calendar.HOUR_OF_DAY) * calendar.get(Calendar.MINUTE) - 112;
        int moveToView = 10 * 60 + calendar.get(Calendar.MINUTE) - 112;
        chart.moveViewToX(moveToView);
        chart.setVisibleXRangeMaximum(60 * 3 + 45);
//        chart.setVisibleXRangeMinimum(60);
        chart.invalidate();
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
    }

    private void setData(int count, float range) {

        values = new ArrayList<>();

        for (int i = 0; i < count; i++) {
            Entry e = new Entry(i, setY(35));
            values.add(e);
        }

        LineDataSet set1;

        if (chart.getData() != null && chart.getData().getDataSetCount() > 0) {
            set1 = (LineDataSet) chart.getData().getDataSetByIndex(0);
            set1.setValues(values);
            chart.getData().notifyDataChanged();
            chart.notifyDataSetChanged();
        } else {
            // create a dataset and give it a type
            set1 = new LineDataSet(values, "");

            set1.setDrawIcons(false);

            set1.setColor(ContextCompat.getColor(this, R.color.clr_line));
            set1.setCircleColor(ContextCompat.getColor(this, R.color.clr_line));
            set1.setLineWidth(1f);
            set1.setCircleRadius(0f);
            set1.setDrawCircles(false);

            set1.setDrawValues(false);
            set1.setValueTextSize(9f);
            set1.setDrawFilled(true);
            set1.setFormLineWidth(1f);
            set1.setFormLineDashEffect(new DashPathEffect(new float[]{10f, 5f}, 0f));
            set1.setFormSize(15.f);
            set1.setMode(LineDataSet.Mode.LINEAR);

            if (Utils.getSDKInt() >= 18) {
                // fill drawable only supported on api level 18 and above
                Drawable drawable = ContextCompat.getDrawable(this, R.drawable.bg_filled);
                set1.setFillDrawable(drawable);
            } else {
                set1.setFillColor(ContextCompat.getColor(this, R.color.clr_line));
            }

            ArrayList<ILineDataSet> dataSets = new ArrayList<>();
            dataSets.add(set1); // add the datasets

            // create a data object with the datasets
            LineData data = new LineData(dataSets);
            // set data
            chart.setData(data);
        }
    }

    @Override
    public void onChartGestureStart(MotionEvent me, ChartTouchListener.ChartGesture lastPerformedGesture) {
//        Log.i("Gesture", "START, x: " + me.getX() + ", y: " + me.getY());
    }

    @Override
    public void onChartGestureEnd(MotionEvent me, ChartTouchListener.ChartGesture lastPerformedGesture) {
//        Log.i("Gesture", "END, lastGesture: " + lastPerformedGesture);

        // un-highlight values after the gesture is finished and no single-tap
        if (lastPerformedGesture != ChartTouchListener.ChartGesture.SINGLE_TAP) {
            chart.highlightValues(null); // or highlightTouch(null) for callback to onNothingSelected(...)
        }
    }

    @Override
    public void onChartLongPressed(MotionEvent me) {
//        Log.i("LongPress", "Chart longpressed.");
    }

    @Override
    public void onChartDoubleTapped(MotionEvent me) {
//        Log.i("DoubleTap", "Chart double-tapped.");
    }

    @Override
    public void onChartSingleTapped(MotionEvent me) {
//        Log.i("SingleTap", "Chart single-tapped.");
    }

    @Override
    public void onChartFling(MotionEvent me1, MotionEvent me2, float velocityX, float velocityY) {
//        Log.i("Fling", "Chart flinged. VeloX: " + velocityX + ", VeloY: " + velocityY);
    }

    @Override
    public void onChartScale(MotionEvent me, float scaleX, float scaleY) {
//        Log.i("Scale / Zoom", "ScaleX: " + scaleX + ", ScaleY: " + scaleY);
    }

    @Override
    public void onChartTranslate(MotionEvent me, float dX, float dY) {
//        Log.i("Translate / Move", "dX: " + dX + ", dY: " + dY);
    }

    @Override
    public void onValueSelected(Entry e, Highlight h) {
        Log.i("Entry selected", e.toString());
        Log.i("LOWHIGH", "low: " + chart.getLowestVisibleX() + ", high: " + chart.getHighestVisibleX());
        Log.i("MIN MAX", "xmin: " + chart.getXChartMin() + ", xmax: " + chart.getXChartMax() + ", ymin: " + chart.getYChartMin() + ", ymax: " + chart.getYChartMax());
    }

    @Override
    public void onNothingSelected() {
//        Log.i("Nothing selected", "Nothing selected.");
    }

    public void initData() {
        float temp = 35.0f;
        for (int i = 0; i < 10; i++) {
            tempDetails.add(new TempDetail(temp + ""));
            temp += 0.5f;
        }
    }

    public static float setY(float y) {
        return 1.08f * y - 1.96f;
    }

    public static float getY(float x) {
        return (x + 1.96f) / 1.08f;
    }
}
